/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-positiveInt>` adds input number to the page. Uses input type number with only positive numbers.
 * In typical use, just use `<fhir-positive-int url=""></fhir-positive-int>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-positiveInt.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-formfield/mwc-formfield.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirPositiveInt extends LitElement {
  static get properties() {
    return {
      /**positiveInt is used to show persons date of birth. Use this property to show/hide. Default: true */
      positiveInt: String,
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String,
      /**value is used to take the input value of each field*/
      value: String
    };
  }

  constructor() {
    super();
    this.positiveInt = 'true';
    this.value = '';
  }

  /**_didRender() delivers only after _render*/
  _didRender() {
    this.shadowRoot
      .getElementById('ajax')
      .addEventListener('iron-ajax-response', function(e) {
        var positiveInt = this.parentNode.host;
        if (e.detail.response.positiveInt !== undefined) {
          positiveInt.shadowRoot.querySelector('.positiveInt').value = 1;
        } else {
          this.parentNode.removeChild(
            this.parentNode.querySelector('#positiveIntDiv')
          );
        }
      });
  }

  _render({ positiveInt, url, value }) {
    return html`
      <div id="positiveIntDiv">
        ${positiveInt !== 'false'
          ? html`
              <mwc-formfield class="positiveInt" alignEnd">
                <input
                  id="positiveInt"
                  type="number"
                  min="0"
                  value="${this.value}"
                  on-input="${e => (this.value = e.target.value)}"
                />
              </mwc-formfield>
            `
          : ''}
      </div>
      <iron-ajax
        id="ajax"
        bubbles
        auto
        handle-as="json"
        url="${url}"
      ></iron-ajax>
    `;
  }
}

window.customElements.define('fhir-positiveint', FhirPositiveInt);
