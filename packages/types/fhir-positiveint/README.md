# fhir-positiveInt

The fhir-positiveInt adds input date to the page that displays the positiveInt. It is commonly used
as a form field. It uses number input and iron-ajax.

### Functionality

Default : shows an empty date input field for `display`
######a. GET:

- It displays date time when it receives a value. `value` can be passed as a string.
- It can also receive value from a 'url' which can be passed as property "url". The `positiveInt` key value is checked for in key-value pair of data.
- Setting `positiveInt` property as true or false can help show and hide this component.
  ######b. SET:
- An input of date value is taken as its value and used for posting purposes.

### Properties of fhir-positiveInt

- `value`:`String` - used to take the input value of each field.
- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.
- `positiveInt`:`String` - used to shows the field. Use this property to show/hide. Default: true.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

1.  Import on the page.
2.  Use the below tags:

- With url:
  `<fhir-allergy-positiveInt url=""></fhir-allergy-positiveInt>`
- Without url:
  `<fhir-allergy-positiveInt></fhir-allergy-positiveInt>`
