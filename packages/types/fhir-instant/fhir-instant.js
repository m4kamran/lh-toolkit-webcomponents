/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-instant>` adds input date to the page. Uses input type datetime in YYYY-MM-DD HH:mm:ss format
 * In typical use, just use `<fhir-instant url=""></fhir-instant>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-instant.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-formfield/mwc-formfield.js';
import '@polymer/iron-ajax/iron-ajax.js';
import moment from 'moment';

class FhirInstant extends LitElement {
  static get properties() {
    return {
      /**Instant is used to show persons date of birth. Use this property to show/hide. Default: true */
      instant: String,
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String,
      /**value is used to take the input value of each field*/
      value: String
    };
  }

  constructor() {
    super();
    this.instant = 'true';
    this.value = '';
  }

  /**_didRender() delivers only after _render*/
  _didRender() {
    this.shadowRoot
      .getElementById('ajax')
      .addEventListener('iron-ajax-response', function(e) {
        var datetime = this.parentNode.host;
        if (e.detail.response.instant !== undefined) {
          datetime.value = moment(e.detail.response.instant).format(
            'YYYY-MM-DD HH:mm:ss'
          );
        } else {
          this.parentNode.removeChild(
            this.parentNode.querySelector('#instantDiv')
          );
        }
      });
  }

  _render({ instant, url, value }) {
    return html`
      <div id="instantDiv">
        ${instant !== 'false'
          ? html`
              <mwc-formfield class="instant" alignEnd>
                <input
                  id="date"
                  type="datetime-local"
                  value="${this.value}"
                  on-input="${e => (this.value = e.target.value)}"
                />
              </mwc-formfield>
            `
          : ''}
      </div>
      <iron-ajax
        id="ajax"
        bubbles
        auto
        handle-as="json"
        url="${url}"
      ></iron-ajax>
    `;
  }
}

window.customElements.define('fhir-instant', FhirInstant);
