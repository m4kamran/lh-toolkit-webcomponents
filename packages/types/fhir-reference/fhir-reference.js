/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-reference>` adds critically of allergy to page to select from code if the critical nature of occurrence was low/moderate/high.
 * Uses select to choose options.
 * In typical use, just use `<fhir-reference url=""></fhir-reference>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-reference.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-textfield/mwc-textfield.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirReference extends LitElement {
  static get properties() {
    return {
      /**reference is a string. Use this property to show/hide. Default: true */
      reference: String,
      /**display is a string. Use this property to show/hide. Default: true */
      display: String,
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String,
      /**value is used to take the input value of each field*/
      value: Object
    };
  }

  /**default value of properties set in constructor*/
  constructor() {
    super();
    this.reference = 'true';
    this.display = 'true';
    this.value = {};
  }

  /**_didRender() delivers only after _render*/
  _didRender() {
    this.shadowRoot
      .getElementById('ajax')
      .addEventListener('iron-ajax-response', function(e) {
        var ref = this.parentNode.host;
        if (e.detail.response.criticality !== undefined) {
          ref.value = e.detail.response.reference;
        } else {
          this.parentNode.removeChild(
            this.parentNode.querySelector('#referenceDiv')
          );
        }
      });
  }

  _render({ reference, display, url, value }) {
    return html`
      <div id="referenceDiv">
        ${reference !== 'false'
          ? html`
              <label>Reference</label>
              <mwc-textfield
                outlined
                class="reference"
                value="${this.value.reference}"
                on-input="${e =>
                  (this.value.reference = e.target._input.value)}"
              ></mwc-textfield>
            `
          : ''}
        ${display !== 'false'
          ? html`
              <label>Display</label>
              <mwc-textfield
                outlined
                class="display"
                value="${this.value.display}"
                on-input="${e => (this.value.display = e.target._input.value)}"
              ></mwc-textfield
              ><br />
            `
          : ''}
      </div>
      <iron-ajax
        id="ajax"
        bubbles
        auto
        handle-as="json"
        url="${url}"
      ></iron-ajax>
    `;
  }
}

window.customElements.define('fhir-reference', FhirReference);
