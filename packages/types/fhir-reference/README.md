# fhir-reference

The fhir-reference adds reference object. It is commonly used
as a form field. It uses input type text for reference and display name. and iron-ajax.

### Functionality

Default : shows an input field for the display name and the reference of the actor.
######a. GET:

- It selects an value from the text entered into the input fields. `value` can be passed as a string.
- It can also receive value from a 'url' which can be passed as property "url". The `reference` key value is checked for in key-value pair of data.
- If it does not receive any matching value, it shows blank.
- Setting `reference` property as true or false can help show and hide this component.
  ######b. SET:

### Properties of fhir-reference

- `value`:`Object` - used to take the input value of each field.
- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.
- `typeField`:`String` - selectable option critical nature of allergy. Use this property to show/hide. Default: true.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

- With url:
  `<fhir-reference url=""></fhir-reference>`
- Without url:
  `<fhir-reference></fhir-reference>`
