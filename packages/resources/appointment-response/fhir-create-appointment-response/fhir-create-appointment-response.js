/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-appointment-response>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-appointment-response url=""></fhir-create-appointment-response>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-appointment-response.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-reference/fhir-reference';
import '@lh-toolkit/fhir-instant/fhir-instant.js';
import '@lh-toolkit/fhir-participant-status/fhir-participant-status.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateAppointmentResponse extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <p>Appointment:</p>
      <fhir-reference id="appointment"></fhir-reference>
      <p>Start:</p>
      <fhir-instant id="start"></fhir-instant>
      <p>End:</p>
      <fhir-instant id="end"></fhir-instant>
      <p>Participant Status</p>
      <fhir-participant-status id="participant"></fhir-participant-status>

      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var appointment = this.shadowRoot.getElementById('appointment').value;
    var start = this.shadowRoot.getElementById('start').value;
    var end = this.shadowRoot.getElementById('end').value;
    var participantStatus = this.shadowRoot.getElementById('participant').value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'AppointmentResponse',
      appointment: appointment,
      start: start,
      end: end,
      participantStatus: participantStatus
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define(
  'fhir-create-appointment-response',
  FhirCreateAppointmentResponse
);
