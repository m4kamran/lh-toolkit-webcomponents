/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-diagnostic-report>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-diagnostic-report url=""></fhir-create-diagnostic-report>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-diagnostic-report.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-reference/fhir-reference';
import '@lh-toolkit/fhir-diagnostic-report-status/fhir-diagnostic-report-status';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateDiagnosticReport extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <p>Subject:</p>
      <fhir-reference id="subject"></fhir-reference>
      <p>Clinical Status:</p>
      <fhir-diagnostic-report-status
        id="status"
      ></fhir-diagnostic-report-status>
      <p>Performer:</p>
      <fhir-reference id="performer"></fhir-reference>
      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var subject = this.shadowRoot.getElementById('subject').value;
    var status = this.shadowRoot.getElementById('status').value;
    var performer = this.shadowRoot.getElementById('performer').value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'DiagnosticReport',
      subject: subject,
      status: status,
      performer: performer
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define(
  'fhir-create-diagnostic-report',
  FhirCreateDiagnosticReport
);
