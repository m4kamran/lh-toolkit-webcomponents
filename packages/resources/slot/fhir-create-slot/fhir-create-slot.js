/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-slot>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-slot url=""></fhir-create-slot>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-slot.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-instant/fhir-instant';
import '@lh-toolkit/fhir-reference/fhir-reference';
import '@lh-toolkit/fhir-slot-status/fhir-slot-status';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateSlot extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <p>Schedule:</p>
      <fhir-reference id="schedule"></fhir-reference>
      <fhir-slot-status id="status"></fhir-slot-status>
      <p>Start:</p>
      <fhir-instant id="start"></fhir-instant>
      <p>End:</p>
      <fhir-instant id="end"></fhir-instant>
      <br />
      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var schedule = this.shadowRoot.getElementById('schedule').value;
    var status = this.shadowRoot.getElementById('status').value;
    var start = this.shadowRoot.getElementById('start').value;
    var end = this.shadowRoot.getElementById('end').value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'Schedule',
      schedule: schedule,
      status: status,
      start: start,
      end: end
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define('fhir-create-slot', FhirCreateSlot);
