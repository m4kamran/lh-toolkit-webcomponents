# fhir-create-slot

The fhir-create-slot creates a form/page to create a new patient with all reusable components like fhir-reference, fhir-instant, fhir-slot-status

### Functionality

Default : shows a group of fields used in the components stated above for `display` along with a "Submit" button.

- Typing in the value in textfield sets the value of the component.
- "url" can be provided using url property to direct where the data is to be posted.
- Pressing the button helps to post data to the desired url passed using url property.
- Data is posted in JSON format with `resourceType` as slot.

### Properties of fhir-create-slot

- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

- With url:
  `<fhir-create-slot url=""></fhir-create-slot>`
- Without url:
  `<fhir-create-slot></fhir-create-slot>`
