/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-schedule>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-schedule url=""></fhir-create-schedule>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-schedule.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-period/fhir-period.js';
import '@lh-toolkit/fhir-reference/fhir-reference';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateSchedule extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <p>Actor:</p>
      <fhir-reference id="actor"></fhir-reference>
      <p>PlanningHorizon:</p>
      <fhir-period
        id="planning"
        end
        value='{"start":"2018-07-01", "end": "2018-10-11"}'
      ></fhir-period>

      <br />
      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var actor = this.shadowRoot.getElementById('actor').value;
    var planning = this.shadowRoot.getElementById('planning').value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'Schedule',
      actor: [actor],
      planningHorizon: planning
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define('fhir-create-schedule', FhirCreateSchedule);
