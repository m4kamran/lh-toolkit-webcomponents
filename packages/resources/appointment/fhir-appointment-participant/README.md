# fhir-appointment-reference

The fhir-appointment-reference adds reference of appointment to page. It is commonly used
as a form field. It uses input type "select" and iron-ajax.It is a coded concept in FHIR and hence hard-coded into the pattern.

### Functionality

Default : shows a selectable input field for `participant status`. Options include : Accepted, Declined, Tentative, Needs Action as options. And also an actor reference
######a. GET:

- It selects and option from available select options when it receives a value. `value` can be passed as a string.
- It can also receive value from a 'url' which can be passed as property "url". The `reference` key value is checked for in key-value pair of data.
- If it does not receive any matching value, it shows blank.
- Setting `typeField` property as true or false can help show and hide this component.
  ######b. SET:
- A selection of option sets the value of the component used for posting purposes.

### Properties of fhir-appointment-reference

- `value`:`Array` - used to take the input value of each field.
- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.
- `typeField`:`String` - selectable option category of allergy. Use this property to show/hide. Default: true.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

- With url:
  `<fhir-appointment-reference url=""></fhir-appointment-reference>`
- Without url:
  `<fhir-appointment-reference></fhir-appointment-reference>`
