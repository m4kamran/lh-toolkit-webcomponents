/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-appointment-status>` adds status of appointment to page. Uses select to choose options.
 * It is a code concept in FHIR and hence hard-coded into the pattern.
 * In typical use, just use `<fhir-appointment-status url=""></fhir-appointment-status>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-appointment-status.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-textfield/mwc-textfield.js';
import '@lh-toolkit/fhir-reference/fhir-reference.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirAppointmentParticipant extends LitElement {
  static get properties() {
    return {
      /**status is a selectable option type of status. Use this property to show/hide. Default: true */
      status: String,
      /**reference is a selectable option type of status. Use this property to show/hide. Default: true */
      reference: String,
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String,
      /**value is used to take the input value of each field*/
      value: Object
    };
  }

  /**default value of properties set in constructor*/
  constructor() {
    super();
    this.status = 'true';
    this.reference = 'true';
    this.value = {};
  }

  /**_didRender() delivers only after _render*/
  _didRender() {
    this.shadowRoot
      .getElementById('ajax')
      .addEventListener('iron-ajax-response', function(e) {
        var appointmentStatus = this.parentNode.host;
        if (e.detail.response.status !== undefined) {
          appointmentStatus.value = e.detail.response.category;
        } else {
          this.parentNode.removeChild(
            this.parentNode.querySelector('#participantDiv')
          );
        }
      });
  }

  _render({ status, reference, url, value }) {
    return html`
      <div id="participantDiv">
        ${status !== 'false'
          ? html`
              <label>Status:</label>
              <select
                class="status"
                value="${this.value.status}"
                on-change="${e => (this.value.status = e.target.value)}"
              >
                <option value="accepted">Accepted</option>
                <option value="declined">Declined</option>
                <option value="tentative">Tentative</option>
                <option value="needs-action">Needs Action</option>
              </select>
            `
          : ''}
        ${reference !== 'false'
          ? html`
              <fhir-reference
                class="reference"
                value="${this.value.actor}"
                on-input="${e => (this.value.actor = e.target.value)}"
              ></fhir-reference>
            `
          : ''}
      </div>
      <iron-ajax
        id="ajax"
        bubbles
        auto
        handle-as="json"
        url="${url}"
      ></iron-ajax>
    `;
  }
}

window.customElements.define(
  'fhir-appointment-participant',
  FhirAppointmentParticipant
);
