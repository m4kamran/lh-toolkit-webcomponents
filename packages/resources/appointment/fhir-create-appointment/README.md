# fhir-create-appointment

The fhir-create-appointment creates a form/page to create a new patient with all reusable components like fhir-instant, fhir-positiveint, fhir-appointment-status, fhir-appointment-participant

### Functionality

Default : shows a group of fields used in the components stated above for `display` along with a "Submit" button.

- Typing in the value in textfield sets the value of the component.
- "url" can be provided using url property to direct where the data is to be posted.
- Pressing the button helps to post data to the desired url passed using url property.
- Data is posted in JSON format with `resourceType` as appointment.

### Properties of fhir-create-appointment

- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

- With url:
  `<fhir-create-appointment url=""></fhir-create-appointment>`
- Without url:
  `<fhir-create-appointment></fhir-create-appointment>`
