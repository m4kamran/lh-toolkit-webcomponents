/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-appointment>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-appointment url=""></fhir-create-appointment>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-appointment.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-appointment-status/fhir-appointment-status';
import '@lh-toolkit/fhir-instant/fhir-instant.js';
import '@lh-toolkit/fhir-positiveint/fhir-positiveint';
import '@lh-toolkit/fhir-appointment-participant/fhir-appointment-participant.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateAppointment extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <fhir-appointment-status id="appointmentStatus"></fhir-appointment-status>
      <p>Start:</p>
      <fhir-instant id="start"></fhir-instant>
      <p>End:</p>
      <fhir-instant id="end"></fhir-instant>
      <p>Minutes Duration:</p>
      <fhir-positiveint id="minutes"></fhir-positiveint>
      <p>Participant:</p>
      <fhir-appointment-participant
        id="participant"
      ></fhir-appointment-participant>

      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var status = this.shadowRoot.getElementById('appointmentStatus').value;

    var start = this.shadowRoot.getElementById('start').value;
    var end = this.shadowRoot.getElementById('end').value;
    var minutes = this.shadowRoot.getElementById('minutes').value;
    var participant = this.shadowRoot.getElementById('participant').value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'appointment',
      status: status,
      start: start,
      end: end,
      minutesDuration: minutes,
      participant: participant
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define('fhir-create-appointment', FhirCreateAppointment);
