/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-create-condition>` creates a form/page to create a new appointment with all reusable components.
 * Uses fhir-components,mwc-button and iron-ajax.
 * In typical use, just use `<fhir-create-condition url=""></fhir-create-condition>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-create-condition.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-button/mwc-button.js';
import '@lh-toolkit/fhir-reference/fhir-reference';
import '@lh-toolkit/fhir-condition-clinicalstatus/fhir-condition-clinicalstatus';
import '@lh-toolkit/fhir-condition-verificationstatus/fhir-condition-verificationstatus';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirCreateCondition extends LitElement {
  static get properties() {
    return {
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String
    };
  }

  _render({ url }) {
    return html`
      <p>Subject:</p>
      <fhir-reference id="subject"></fhir-reference>
      <p>Clinical Status:</p>
      <fhir-condition-clinicalstatus
        id="clinicalstatus"
      ></fhir-condition-clinicalstatus>
      <p>Verification Status:</p>
      <fhir-condition-verificationstatus
        id="verificationstatus"
      ></fhir-condition-verificationstatus>

      <br />
      <mwc-button id="button" raised on-click=${() => this.doPost()}
        >Submit</mwc-button
      >
      <iron-ajax
        bubbles
        method="POST"
        id="ajax"
        url="${url}"
        on-response="handleResponse"
      ></iron-ajax>
    `;
  }
  doPost() {
    var subject = this.shadowRoot.getElementById('subject').value;
    var clinicalstatus = this.shadowRoot.getElementById('clinicalstatus').value;
    var verificationstatus = this.shadowRoot.getElementById(
      'verificationstatus'
    ).value;

    this.shadowRoot.getElementById('ajax').contentType = 'application/json';
    this.shadowRoot.getElementById('ajax').body = {
      resourceType: 'Condition',
      subject: subject,
      verificationStatus: verificationstatus,
      clinicalStatus: clinicalstatus
    };
    this.shadowRoot.getElementById('ajax').generateRequest();
  }
}
window.customElements.define('fhir-create-condition', FhirCreateCondition);
