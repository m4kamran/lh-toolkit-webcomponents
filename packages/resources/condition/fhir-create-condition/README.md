# fhir-create-condition

The fhir-create-condition creates a form/page to create a new patient with all reusable components like fhir-reference, fhir-condition-verificationstatus. fhir-condition-clinicalnstatus

### Functionality

Default : shows a group of fields used in the components stated above for `display` along with a "Submit" button.

- Typing in the value in textfield sets the value of the component.
- "url" can be provided using url property to direct where the data is to be posted.
- Pressing the button helps to post data to the desired url passed using url property.
- Data is posted in JSON format with `resourceType` as AppointmentResponse.

### Properties of fhir-create-condition

- `url`:`String` - used to make AJAX call to FHIR resource. Default: null.

### License

Mozilla Public License, v. 2.0.

### Typical Use:

- With url:
  `<fhir-create-condition-response url=""></fhir-create-condition-response>`
- Without url:
  `<fhir-create-condition-response></fhir-create-condition-response>`
